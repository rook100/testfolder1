SET DATABASE=BLOOM
SET SERVER=WIN2K8R2\SQL2008R2
SET USER=sa
SET PASSWORD=Mib6422
SET TABLE=vendaddr

ECHO ----------------- >> %TABLE%log.txt
ECHO . >> %TABLE%log.txt
ECHO %TABLE% >> %TABLE%log.txt
bcp "select ltrim(replace(p.[VENDORID],',','#*')), coalesce(replace(a.adrscode,',','#'),'') adrscode, replace(p.VNDCNTCT,',','#*'),replace(p.VENDNAME,',','#*'),coalesce(replace(a.ADDRESS1,',','#*'),''),coalesce(replace(a.ADDRESS2,',','#*'),''),coalesce(replace(a.ADDRESS3,',','#*'),''),coalesce(replace(a.CITY,',','#*'),''),coalesce(replace(a.STATE,',','#*'),''),coalesce(a.ZIPCODE,''),coalesce(replace(a.COUNTRY,',','#*'),''),case when substring(p.PHNUMBR1,1,10) <> '0000000000' then substring(p.PHNUMBR1,1,10) else '' end phone1, case when substring(p.PHNUMBR1,11,4) <> '0000' then substring(p.PHNUMBR1,11,4) else '' end phone1ext,case when substring(p.PHNUMBR2,1,10) <> '0000000000' then substring(p.PHNUMBR2,1,10) else '' end phone2, case when substring(p.PHNUMBR2,11,4) <> '0000' then substring(p.PHNUMBR2,11,4) else '' end phone2ext,case when substring(p.PHONE3,1,10) <> '0000000000' then substring(p.PHONE3,1,10) else '' end phone3, case when substring(p.PHONE3,11,4) <> '0000' then substring(p.PHONE3,11,4) else '' end phone3ext,case when substring(p.FAXNUMBR,1,10) <> '0000000000' then substring(p.FAXNUMBR,1,10) else '' end FAX, case when substring(p.FAXNUMBR,11,4) <> '0000' then substring(p.FAXNUMBR,11,4) else '' end faxext from %DATABASE%..pm00200 p join %DATABASE%..vPM20000 v on p.VENDORID = v.vendorid left outer join %DATABASE%..pm00300 a on p.VENDORID = a.VENDORID" queryout %TABLE%.csv -b 1000 -c -t "|" -S %SERVER% -T >> %TABLE%log.txt