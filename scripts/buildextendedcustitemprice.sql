select linkcode,prcshid into #tmpcustprice from rm00500 where PRODTCOD = 'C' order by LINKCODE

--select * from #tmpcustprice

select t.LINKCODE, h.PRCSHID, h.ITEMNMBR, d.UOFM, d.QTYFROM, d.QTYTO, h.PRODTCOD
, case when h.PRODTCOD = 'N' then d.PSITMVAL when h.PRODTCOD = 'P' then 0.00 else -1.00 end price  -- PRODTCOD N=netprice P=percent
, case when h.PRODTCOD = 'P' then d.PSITMVAL/100.00 else 0.00 end pct
, s.STRTDATE, s.ENDDATE
, IDENTITY(INT,1,1) seq
into #tmpcustprice2
from #tmpcustprice t join sop10110 s on t.PRCSHID = s.PRCSHID
join iv10401 h on t.PRCSHID = h.PRCSHID
join iv10402 d on h.PRCSHID = d.PRCSHID and h.ITEMNMBR = d.ITEMNMBR
where 1=1
order by h.PRCSHID,h.ITEMNMBR
--select * from #tmpcustprice2 where PRODTCOD = 'P'
if ((select count(*) from #tmpcustprice2 where PRODTCOD = 'P' and price = 0) > 0)
begin
	DECLARE @stored_proc_name char(47), @retstat int, @param12 numeric(19,5), @param13 smallint , @rows bigint, @count bigint, @uofm char(9), @itemnmbr char(31), @seq bigint, @date char(10)
	SELECT @stored_proc_name = 'dbo.tcsSOPSP00048_EPE_CU_GetPriceSheetItem', @param12 = 0.00000, @param13 = 0, @count = 0, @date = '03/18/2020'
	declare curitem scroll cursor for select ITEMNMBR, uofm, seq from #tmpcustprice2 where PRODTCOD = 'P' and price = 0
	open curitem
	fetch next from curitem into @itemnmbr, @uofm, @seq
	while @@FETCH_STATUS <> -1
	begin
		EXEC @retstat = @stored_proc_name 'P', '', @itemnmbr, @uofm, @date, 1.00000, 'Z-US$', 0, 'BASESHEET', 0, '', @param12 OUT, @param13 OUT 	
		update #tmpcustprice2 set price = @param12 * pct where seq = @seq
		fetch next from curitem into @itemnmbr, @uofm, @seq
	end
	close curitem
	deallocate curitem
end
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zcustitemprice]') AND type in (N'U'))
DROP TABLE [dbo].[zcustitemprice]


select * into zcustitemprice from #tmpcustprice2 order by LINKCODE,PRCSHID,ITEMNMBR
drop table #tmpcustprice
drop table #tmpcustprice2

select * from zcustitemprice where LINKCODE = '461616' and PRCSHID = 'FRUIT LEATHER'

