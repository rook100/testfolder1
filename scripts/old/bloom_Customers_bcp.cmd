SET DATABASE=BLOOM
SET SERVER=WIN2K8R2\SQL2008R2
SET USER=sa
SET PASSWORD=Mib6422
SET TABLE=customers

ECHO ----------------- >> %TABLE%log.txt
ECHO . >> %TABLE%log.txt
ECHO %TABLE% >> %TABLE%log.txt
bcp "select r.custnmbr entityId,r.cprcstnm parenthandle,replace(r.custname,',','#*') companyName,r.custclas category,r.slprsnid salesrepref, coalesce(replace(s.INET1,',','#*'),'') email,case when substring(r.PHONE1,1,10) <> '0000000000' then substring(r.PHONE1,1,10) else '' end phone, case when substring(r.PHONE1,11,4) <> '0000' then substring(r.PHONE1,11,4) else '' end phoneext,case when substring(r.PHONE2,1,10) <> '0000000000' then substring(r.PHONE2,1,10) else '' end phone2, case when substring(r.PHONE2,11,4) <> '0000' then substring(r.PHONE2,11,4) else '' end phone2ext,case when substring(r.PHONE3,1,10) <> '0000000000' then substring(r.PHONE3,1,10) else '' end phone3, case when substring(r.PHONE3,11,4) <> '0000' then substring(r.PHONE3,11,4) else '' end phone3ext,case when substring(r.FAX,1,10) <> '0000000000' then substring(r.FAX,1,10) else '' end fax, case when substring(r.fax,11,4) <> '0000' then substring(r.fax,11,4) else '' end faxext,replace(r.comment1,',','#*') comment1,replace(r.comment2,',','#*') comment2,coalesce(REPLACE(REPLACE(REPLACE(replace(cast(n.TXTFIELD as varchar(max)),',','#*'), CHAR(9), ''), CHAR(10), ''), CHAR(13), ''),'') note,r.PYMTRMID,r.crlmttyp	,r.crlmtamt,r.TAXEXMT1 taxexempt1,r.TAXEXMT2 taxexempt2,r.TXRGNNUM taxregistration,r.PRCLEVEL pricelevelref,r.CURNCYID currencyref,r.TAXSCHID taxid,r.INACTIVE,replace(r.cntcprsn,',','#*') from %DATABASE%..rm00101 r left outer join %DATABASE%..SY03900 n on r.NOTEINDX = n.NOTEINDX left outer join %DATABASE%..SY01200 s on r.CUSTNMBR = s.Master_ID and s.Master_Type = 'CUS' join %DATABASE%..vrm20101 v on r.CUSTNMBR = v.custnmbr" queryout %TABLE%.csv -b 1000 -c -t "|" -S %SERVER% -T >> %TABLE%log.txt

