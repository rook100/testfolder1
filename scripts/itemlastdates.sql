select * from POP30300 where POPRCTNM = 'RCT0007869'
select * from POP30310 where POPRCTNM = 'RCT0007869' and ITEMNMBR = '00333'


select * from ReceivingsTransactions 
select * from ReceivingsLineItems where [POP Receipt Number] = 'RCT0007869' and [Item Number]= '00333'
/*
WITH TT AS (
    SELECT *, ROW_NUMBER() OVER(PARTITION BY USER_ID ORDER BY DOCUMENT_DATE DESC) AS N
    FROM test_table
    select ITEMNMBR, 
    from POP30310 d join POP30300 h on h. sp_help pop30300
    where POPRCTNM = 'RCT0007869' and ITEMNMBR = '00333'
)
SELECT *
FROM TT
WHERE N = 1;
*/
--select * from SalesLineItems where [Item Number] = '00333'
/*
SELECT [Item Number], [Invoice Date], [Customer Number], [Unit Price]
    FROM (
        SELECT [Item Number], [Invoice Date], [Customer Number], [Unit Price],
            Rank() over (Partition BY [Item Number] ORDER BY [Invoice Date] DESC, [SOP Number] DESC) AS Rank
        FROM SalesLineItems
WHERE [SOP Type] = 'Invoice' and [Invoice Date] >= '05/01/2020'  --dateand [Item Number] = '00333'
        ) as SalesLineItems WHERE Rank <= 1
ORDER BY 1
*/
--drop view itemsold
create view itemsold
as 
SELECT [Item Number], [Invoice Date]
    FROM (
        SELECT [Item Number], [Invoice Date], Rank() over (Partition BY [Item Number] ORDER BY [Invoice Date] DESC, [SOP Number] DESC) AS Rank
        FROM SalesLineItems
		WHERE [SOP Type] = 'Invoice' --and [Invoice Date] >= '05/01/2020'  --dateand [Item Number] = '00333'
        ) as SalesLineItems WHERE Rank <= 1
--ORDER BY 1
--drop view itemrecived
create view itemrecived
as
select [Item Number],[Receipt Date]
	From (
		select [Item Number],[Receipt Date], Rank() over (Partition BY [Item Number] ORDER BY [Receipt Date] DESC, [POP Receipt Number] DESC) AS Rank 
		from [ReceivingsLineItems] where [POP type] <> 'Return w/Credit' --and [Receipt Date] >= '05/01/2020'
		) as a where RANK <= 1
--order by 1

select d.ITEMNMBR, h.INVODATE
from sop10200 d join sop10100 h on h.SOPTYPE = d.SOPTYPE and h.SOPNUMBE = d.SOPNUMBE and h.SOPTYPE = 3
where ITEMNMBR = '00333' and h.VOIDSTTS = 0
order by h.INVODATE desc

select d.ITEMNMBR, h.INVODATE
from sop30300 d join sop30200 h on h.SOPTYPE = d.SOPTYPE and h.SOPNUMBE = d.SOPNUMBE and h.SOPTYPE = 3
where d.ITEMNMBR = '00333' and h.VOIDSTTS = 0
order by h.INVODATE desc


