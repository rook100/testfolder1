drop view vrm20101
create view vrm20101
as
	select CUSTNMBR	--,CPRCSTNM,DOCNUMBR,CHEKNMBR,BACHNUMB,BCHSOURC,TRXSORCE,RMDTYPAL,CSHRCTYP,DUEDATE,DOCDATE,POSTDATE,PSTUSRID,GLPOSTDT,LSTEDTDT,LSTUSRED,ORTRXAMT,CURTRXAM,SLSAMNT,COSTAMNT,FRTAMNT,MISCAMNT,TAXAMNT,COMDLRAM,CASHAMNT,DISTKNAM,DISAVAMT,DISCRTND,DISCDATE,DSCDLRAM,DSCPCTAM,WROFAMNT,TRXDSCRN,CSPORNBR,SLPRSNID,SLSTERCD,DINVPDOF,PPSAMDED,GSTDSAMT,DELETE1,VOIDSTTS,VOIDDATE,TAXSCHID,CURNCYID,PYMTRMID,SHIPMTHD,TRDISAMT,SLSCHDID,FRTSCHID,MSCSCHID,NOTEINDX,Tax_Date,APLYWITH,SALEDATE,CORRCTN,SIMPLIFD,Electronic,ECTRX,BKTSLSAM,BKTFRTAM,BKTMSCAM,BackoutTradeDisc,Factoring,DIRECTDEBIT,ADRSCODE,EFTFLAG,DEX_ROW_TS,DEX_ROW_ID 
	from RM20101
	where docdate >= '01/01/2018'
	group by custnmbr
	union
	select CUSTNMBR --,CPRCSTNM,DOCNUMBR,CHEKNMBR,BACHNUMB,BCHSOURC,TRXSORCE,RMDTYPAL,CSHRCTYP,DUEDATE,DOCDATE,POSTDATE,PSTUSRID,GLPOSTDT,LSTEDTDT,LSTUSRED,ORTRXAMT,CURTRXAM,SLSAMNT,COSTAMNT,FRTAMNT,MISCAMNT,TAXAMNT,COMDLRAM,CASHAMNT,DISTKNAM,DISAVAMT,DISCRTND,DISCDATE,DSCDLRAM,DSCPCTAM,WROFAMNT,TRXDSCRN,CSPORNBR,SLPRSNID,SLSTERCD,DINVPDOF,PPSAMDED,GSTDSAMT,DELETE1,VOIDSTTS,VOIDDATE,TAXSCHID,CURNCYID,PYMTRMID,SHIPMTHD,TRDISAMT,SLSCHDID,FRTSCHID,MSCSCHID,NOTEINDX,Tax_Date,APLYWITH,SALEDATE,CORRCTN,SIMPLIFD,Electronic,ECTRX,BKTSLSAM,BKTFRTAM,BKTMSCAM,BackoutTradeDisc,Factoring,DIRECTDEBIT,ADRSCODE,EFTFLAG,DEX_ROW_TS,DEX_ROW_ID 
	from RM30101
	where docdate >= '01/01/2018'
	group by custnmbr
drop view vpm20000	
create view vPM20000
as
	select vendorid from PM20000
	where docdate >= '01/01/2018'
	group by vendorid
	union
	select vendorid from PM30200
	where docdate >= '01/01/2018'
	group by vendorid