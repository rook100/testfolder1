select i.ITEMNMBR itemid
,i.ITEMNMBR name
, '' displayname	-- see salesdescription 
,coalesce(replace(i3.VNDITNUM,',','#*'),'') vendorname
, '' parentref		-- na						-- 5
,coalesce(replace(i3.VNDITDSC,',','#*'),'') purchasedescription
,i.CURRCOST cost
,coalesce(G5.actnumst,'') costaccountref
, '' vendorref	-- na
,replace(i2.PRIMVNDR,',','#*') preferredvendor	-- 10
, replace(i.ITEMDESC,',','#*') salesDescription
, '' incomeAccountRef	-- na
, '' taxcoderef		-- na
, '' purchasetaxcoderef	-- na
,0.0 salesprice		-- na						-- 15
,'' assetaccountref	-- na
,0.0 reorderpoint	-- na
, 0.0 preferredstocklevel	-- na
, '' itemLocationLine1_locationRef	-- na
, '' itemLocationLine1_reorderPoint	-- na		-- 20
, '' itemLocationLine1_preferredStockLevel	-- na
, '' departmentRef	-- na
,i.ITMCLSCD class
, 0.0 shippingcost	-- na
, 0.0 handlingcost	-- na						-- 25
,i.ITEMSHWT/100.00 weight
, '' weightunitref	-- na
--,i2.QTYONHND quantityonhand
, case when i.vctnmthd = 1 then 'FIFO PERPETUAL' when i.vctnmthd = 2 then 'LIFO PERPETUAL' when i.vctnmthd = 3 then 'AVG' when i.vctnmthd = 4 then 'FIFO PERIODIC' when i.vctnmthd = 5 then 'LIFO PERIODIC' else '' end costingMethodRef
, case when i.TAXOPTNS = 1 then 'TRUE' else 'FALSE' end istaxable		
, case when i.INACTIVE = 1 then 'TRUE' else 'FASLE' end isinactive		-- 30
--,i.Purchase_Tax_Options
,i.UOMSCHDL unitstyperef
,'' stockunitref	-- na
,i.SELNGUOM saleunitref
,i.PRCHSUOM purchaseunitref								
,i.ITMSHNAM upccode													-- 35
,bi.cupcouter outsideupc
,bi.cupcinner innerupc
,i.ITMGEDSC bin
,i.WRNTYDYS zoneitemwarranty
,'' salesinventory													-- 40
,'' seecolumnclass
,si.INET1 rabbisup1
,si.inet2 rabbisup2
,si.inet3 rabbisup3	
, '' rabbisup4											-- 45
,si.INET4 logo
,si.INET5 piecescase
,si.inet8 itemputup
,i.USCATVLS_1 seq1
,i.USCATVLS_2 seq2										-- 50
,i.USCATVLS_3 seq3
,i.USCATVLS_4 seq4
,convert(varchar(10),coalesce(is2.[Invoice Date],''),101) lastsold
,is2.[Invoice Date]
,coalesce(ir.[Receipt Date],'') lastreceived
,ir.[Receipt Date]
from iv00101 i 
left outer join BPI00101 bi on i.ITEMNMBR = bi.ITEMNMBR
left outer join itemsold is2 on i.itemnmbr = is2.[Item Number]
left outer join itemrecived ir on i.ITEMNMBR = ir.[Item Number]
left outer join SY01200 si on si.Master_Type = 'ITM' and si.Master_ID = i.ITEMNMBR
left outer join iv00102 i2 on i.ITEMNMBR = i2.ITEMNMBR and i2.LOCNCODE = '' 
left outer join iv00103 i3 on i.ITEMNMBR = i3.ITEMNMBR and i2.PRIMVNDR = i3.VENDORID 
left outer join gl00100 G1 on i.IVCOGSIX = g1.actindx 
left outer join gl00105 G5 on g1.ACTINDX = g5.ACTINDX

--%DATABASE%..

