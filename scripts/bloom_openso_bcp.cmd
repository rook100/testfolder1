SET DATABASE=BLOOM
SET SERVER=WIN2K8R2\SQL2008R2
SET USER=sa
SET PASSWORD=Mib6422
SET TABLE=openso

ECHO ----------------- >> %TABLE%log.txt
ECHO . >> %TABLE%log.txt
ECHO %TABLE% >> %TABLE%log.txt
bcp "SELECT h.SOPNUMBE tranid, CONVERT(VARCHAR(10),h.DOCDATE,101) trandate, h.PYMTRMID, h.LOCNCODE, h.CUSTNMBR, h.CSTPONBR po, coalesce(REPLACE(REPLACE(REPLACE(replace(cast(c.CMMTTEXT as varchar(max)),',','#*'), CHAR(9), ''), CHAR(10), ''), CHAR(13), ''),'') + ' ' + coalesce(REPLACE(REPLACE(REPLACE(replace(cast(n.TXTFIELD as varchar(max)),',','#*'), CHAR(9), ''), CHAR(10), ''), CHAR(13), ''),'') memo, h.SHIPMTHD, h.FRTAMNT, h.MISCAMNT, h.SLPRSNID, d.lnitmseq, d.cmpntseq, d.itemnmbr, d.NONINVEN, d.DROPSHIP, d.UOFM, d.LOCNCODE,d.UNITPRCE, d.XTNDPRCE,d.MRKDNAMT, d.MRKDNPCT, d.TAXAMNT, d.TRDISAMT, d.QUANTITY, case when d.DROPSHIP = 1 then replace(d.ShipToName,',','#*') else '' end shiptoname, case when d.DROPSHIP = 1 then replace(d.CNTCPRSN,',','#*') else '' end contact, case when d.DROPSHIP = 1 then REPLACE(d.ADDRESS1,',','#*') else '' end addr1, case when d.DROPSHIP = 1 then REPLACE(d.ADDRESS2,',','#*') else '' end addr2, case when d.DROPSHIP = 1 then REPLACE(d.ADDRESS3,',','#*') else '' end addr3, case when d.DROPSHIP = 1 then replace(d.CITY,',','#*') else '' end city, case when d.DROPSHIP = 1 then replace(d.STATE,',','#*') else '' end state, case when d.DROPSHIP = 1 then d.ZIPCODE else '' end zipcode, case when d.DROPSHIP = 1 then d.PHONE1 else '' end phone1, case when d.DROPSHIP = 1 then d.PHONE2 else '' end phone2, case when d.DROPSHIP = 1 then d.PHONE3 else '' end phone3, case when d.DROPSHIP = 1 then d.FAXNUMBR else '' end fax, d.DEX_ROW_ID FROM %DATABASE%..SOP10100 h join %DATABASE%..sop10200 d on h.SOPTYPE = d.SOPTYPE and h.SOPNUMBE = d.SOPNUMBE left outer join %DATABASE%..SOP10106 c on h.SOPTYPE = c.SOPTYPE and h.SOPNUMBE = c.SOPNUMBE left outer join %DATABASE%..SY03900 n on h.NOTEINDX = n.NOTEINDX WHERE h.SOPTYPE = 2 and h.VOIDSTTS = 0 ORDER BY h.CUSTNMBR ASC, h.CSTPONBR ASC, h.DOCDATE ASC, h.DEX_ROW_ID ASC" queryout %TABLE%.csv -b 1000 -c -t "," -S %SERVER% -T >> %TABLE%log.txt

